package org.example.inheritance;


public class Parent
{
  private Integer parentCount = 5;

  public Parent()
  {
    log();
  }


  protected void log()
  {
    System.out.println("Parent Count=" + parentCount.toString());
  }


  static class Child extends Parent
  {
    private Integer childCount;

    public Child()
    {
      childCount = 7;
      log();
    }


    @Override
    protected void log()
    {
      System.out.println("Child value="+ childCount.toString());
    }
  }


  public static void main(String[] args)
  {
    new Child();
  }
}

