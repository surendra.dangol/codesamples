package org.example.report;

import java.io.*;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;


public class ReportRenderer
{
  public static final int FORMAT_CSV = 1;
  public static final int FORMAT_XML = 2;
  public static final int FORMAT_HTML = 3;
  private static final int FORMAT_JSON = 4;
  private int format;
  private String reportName;
  private String title;
  private List<String> headers;
  private List<List<String>> data;
  private OutputStream fileStream;


  public ReportRenderer(int format, String reportName, String title, List<String> headers, List<List<String>> data)
  {
    this.format = format;
    this.reportName = reportName;
    this.title = title;
    this.headers = headers;
    this.data = data;
    render();
  }

  private void render()
  {
    try {
      fileStream = new FileOutputStream(reportName + "." + getExtension());
      prepareReport();
      writeHeader(headers);
      data.forEach(this::writeRow);
      finalizeReport();
      fileStream.close();
    }
    catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void prepareReport()
  {
    String content = "";
    switch (format) {
      case FORMAT_CSV:
        //no logic
        break;
      case FORMAT_XML:
        content = "<report>\n" +
            "<title>"+title+"</title>\n";
        break;
      case FORMAT_HTML:
        content = "<html>\n" +
            "<head><title>"+title+"</title></head>\n" +
            "<body>\n" +
            "<table>\n";
        break;
    }
    writeToFile(content);
  }

  private void writeToFile(String content)
  {
    try {
      fileStream.write(content.getBytes());
    }
    catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void writeHeader(List<String> headers)
  {
    String content = "";
    switch (format) {
      case FORMAT_CSV:
        content = String.join(";", headers) + "\n";
        break;
      case FORMAT_XML:
        content += "<headers>\n";
        content += headers.stream()
            .map(header -> "<header>" + header + "</header>")
            .collect(Collectors.joining("\n"));
        content += "\n</headers>\n";
        content += "<values>\n";
        break;
      case FORMAT_HTML:
        content += "<tr>\n";
        content += headers.stream()
            .map(header -> "<th>" + header + "</th>")
            .collect(Collectors.joining("\n"));
        content += "\n</tr>\n";
        break;
    }
    writeToFile(content);
  }



  public void writeRow(List<String> rowValues)
  {
    String content = "";
    switch (format) {
      case FORMAT_CSV:
        content = String.join(";", rowValues) + "\n";
        break;
      case FORMAT_XML:
        content += "<row>\n";
        content += rowValues.stream()
            .map(cell -> "<cell>" + cell + "</cell>")
            .collect(Collectors.joining("\n"));
        content += "\n</row>\n";
        break;
      case FORMAT_HTML:
        content += "<tr>\n";
        content += rowValues.stream()
            .map(cell -> "<td>" + cell + "</td>")
            .collect(Collectors.joining("\n"));
        content += "\n</tr>\n";
        break;
    }
    writeToFile(content);
  }

  public void finalizeReport()
  {
    String content = "";
    switch (format) {
      case FORMAT_CSV:
        //No logic
        break;
      case FORMAT_XML:
        content += "</values>\n" +
            "</report>\n";
        break;
      case FORMAT_HTML:
        content += "</table>\n" +
            "</body>\n" +
            "</html>\n";
        break;
    }
    writeToFile(content);
  }

  public String getExtension()
  {
    switch (format) {
      case FORMAT_CSV:
        return "csv";
      case FORMAT_XML:
        return "xml";
      case FORMAT_HTML:
        return "html";
    }
    return null;
  }


  public static void main(String[] args)
  {
    List<String> headers = List.of("Beneficiary", "Debit account", "Amount");
    Random random = new Random();
    List<List<String>> values = IntStream.range(1, 101)
        .mapToObj(i -> List.of("Company " + i, "ACC " + (i), String.valueOf(random.nextInt(10_000))))
        .collect(Collectors.toList());

    new ReportRenderer(3, "payment2020", "Executed Payments 2020", headers, values);
  }
}